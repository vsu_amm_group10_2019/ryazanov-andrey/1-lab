﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTree
{
    public class Node
    {
        const string glasn = "aeioyuаеёиоуыэюя";
        const string soglasn = "qwrtpsdfghjklzxcvbnmбвгджзйклмнпрстфхцчшшщ";
        public Dictionary<char, Node> Childs { get; set; } = new Dictionary<char, Node>();
        public int Count { get; set; }
        public void Add(string value)
        {
            if (value.Length == 0)
            {
                Count++;
                return;
            }
            char next = value[0];
            string remaining = value.Length == 1 ? "" : value.Substring(1);

            if (!Childs.ContainsKey(next))
            {
                Node node = new Node();
                Childs.Add(next, node);
            }
            Childs[next].Add(remaining);
        }
        public bool Find(string value)
        {
            if (value.Length == 0)
            {
                return Count>0;
            }
            char next = value[0];
            string remaining = value.Length == 1 ? "" : value.Substring(1);
            if (!Childs.ContainsKey(next))
            {
                return false;
            }
            return Childs[next].Find(remaining);
        }
        public Dictionary<string,int> Calculate(string start)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            if (Count > 0)
            {
                result.Add(start, Count);
            }
            foreach (var keyValuePair in Childs)
            {
                Dictionary<string,int> tmp = keyValuePair.Value.Calculate(start + keyValuePair.Key);
                foreach(var kvp in tmp)
                {
                    if (WordsCheck(kvp.Key)) //Если количество согласных и гласных в слове равно, то добавляем его
                    {
                        result.Add(kvp.Key,kvp.Value);
                    }
                    else Count--; 
                }
            }
            return result;
        } 
        public void PrintToTreeNode (TreeNode treeNode)
        {
            int i = 0;
            foreach (var keyValuePair in Childs)
            {
                treeNode.Nodes.Add(keyValuePair.Key.ToString());
                keyValuePair.Value.PrintToTreeNode(treeNode.Nodes[i]);
                i++;
            }
        }
        public bool Delete (string word)
        {
            if (word == "")
            {
                if (Childs.Count != 0)
                {
                    Count = 0;
                    return false;
                }
                return true;
            }
            else
            {
                char next = word[0];
                string remaining = word.Length == 1 ? "" : word.Substring(1);
                
                if (Childs.ContainsKey(next))
                {
                    if (Childs[next].Delete(remaining))
                    {
                        Childs.Remove(next);
                        if (Childs.Count > 0)
                        {
                            return false;
                        }
                        return true;
                    }
                }
            }
            return false;
        }
        //Проверка равно ли количество согласных и гласных
        public bool WordsCheck(string word)
        {
            bool result = false;
            int countglasn = 0;
            int countsoglasn = 0;
            int i = 0;
            while (i < word.Length)
            {
                if (soglasn.Contains(word[i]))
                {
                    countsoglasn++;
                }
                if (glasn.Contains(word[i]))
                {
                    countglasn++;
                }
                i++;
            }
            if (countglasn > 0 && countglasn == countsoglasn)
                result = true;
            return result;
        }

    }
}
